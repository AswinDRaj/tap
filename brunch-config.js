module.exports = {
    // See http://brunch.io for documentation.
    files: {
        javascripts: {
            joinTo: {
                'vendor.js': /^(?!app)/, // Files that are not in `app` dir.
                'app.js': /^app/
            },
            order : {
                before : [                  
                    'js/app.js'
                ]
            }
        },
        stylesheets: {
            joinTo: 'app.css',
            order: {
                before : [
                    'css/main.css',
                    'css/responsive.css'
                ]
            }
        },
        templates: {
            joinTo: 'js/app.js'
        }
    },
    modules: {
        wrapper: false,
        definition: false
    },
    npm: {
        enabled: false
    }
}

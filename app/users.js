var userModule = angular.module('userModule', ['ngRoute','ngCookies','ngResource']);
	userModule.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
	app.config(['$httpProvider', function($httpProvider) {
		$httpProvider.defaults.headers.post['Content-Type'] = ''
            + 'application/json; charset=UTF-8';
		$httpProvider.defaults.headers.put['Content-Type'] = ''
            + 'application/json; charset=UTF-8';
         
	}]);
	userModule.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$locationProvider.html5Mode(true).hashPrefix('!');
			$routeProvider.
			when('/login', {
				title: 'Login',
				templateUrl: 'login.html',
				controller: 'loginController'
		  	}).
			when('/', {
				title: 'Login',
				templateUrl: 'login.html',
				controller: 'loginController'
		  	}).
			when('/create', {
				title: 'Create',
				templateUrl: 'reate.html',
				controller: 'loginController'
		  	}).			
			when('/my_account', {
				title: 'My Account',
				templateUrl: 'my_account.html',
				controller: 'loginController'
		  	}).
			when('/change_password', {
				title: 'Change Password',
				templateUrl: 'change_password.html',
				controller: 'loginController'
		  	}).
			when('/forgotpass', {
				title: 'Forgot Password',
				templateUrl: 'forgotpass.html',
				controller: 'loginController'
		  	}).
			when('/reset_password', {
				title: 'Reset Password',
				templateUrl: 'reset_password.html',
				controller: 'loginController'
		  	}).
			when('/user_activation', {
				title: 'User Activation',
				templateUrl: 'user_activation.html',
				controller: 'loginController'
		  	});
		}
	]);
	userModule.controller('loginController',['$rootScope', '$routeParams', '$scope', '$http', '$location', '$timeout', '$window',  function ($rootScope, $routeParams, $scope, $http, $location, $timeout, $window) {
		$scope.activation = {};
		$rootScope.isLoggedIn = false;
		$scope.loginSubmit = 'Login';
		$scope.login = function(custId, password) {

			if(!$scope.isSending){
				$scope.isSending = true;
				$scope.loginSubmit = 'Authentication Processing...';
				$http.post(baseUrl+'/user/login', {name: custId, pass: password})
				.success(function (response) {
					$scope.isSending = false;
					$rootScope.isLoggedIn = true;
					$rootScope.userRole = response.data.uid;
					$rootScope.username = response.data.name;
					setLocalStorage(response.data.csrf_token, response.data.uid, response.data.name,response.data.logout_token);
					api.init(response.data.csrf_token);
					console.log(response);
					$location.path('/dashboard');
				}).error(function(data, status){
					$scope.isSending = false;
					$scope.loginErr = true;
					$scope.loginSubmit = 'Login';
					$scope.loginErrText = data.msg;
				});

			
			}
		};
}]);